# Домашнее задание к занятию "3.9. Элементы безопасности информационных систем"

> 1. Установите Bitwarden плагин для браузера. Зарегестрируйтесь и сохраните несколько паролей.

Установил плагин для браузера firefox и зарегшистрировался.

![Plugin Bitwarden](bitwarden_plugin.png?raw=true "Plugin Bitwarden")

> 2. Установите Google authenticator на мобильный телефон. Настройте вход в Bitwarden акаунт через Google authenticator OTP.

Настроил двухфакторную аутентификацию через Google authenticator:

![2FA by Google authenticator OTP](bitwarden_2fa_enabled.png?raw=true "2FA by Google authenticator OTP")

> 3. Установите apache2, сгенерируйте самоподписанный сертификат, настройте тестовый сайт для работы по HTTPS.

1. Устанавливаем apache2: `sudo apt install apache2`
2. Включаем поддержку ssl и перезапускаем сервис:
	```
	sudo a2enmod ssl
	sudo systemctl restart apache2
	```
3. Генерируем самоподписанный сертификат: 
   1. Вводим команду: `openssl req -new -x509 -days 30 -keyout server.key -out server.pem`
   2. На вопрос «Enter PEM pass phrase:» отвечаем паролем, подтверждаем и запоминаем
   3. На вопрос «Common Name (eg, YOUR name) []:» пишем test-ssl.devops.local
   4. Далее снимаем пароль с ключа: `openssl rsa -in server.key.orig -out server.key`
   5. Копируем сертификат и приватный ключ и назначим файлу ключа права чтения только администратору:
		```
		sudo cp server.pem /etc/ssl/certs/
		sudo cp server.key /etc/ssl/private/
		sudo chmod 0600 /etc/ssl/private/server.key
		```
4. Создаём конфигурацию для сайта:

```
sudo vi /etc/apache2/sites-available/test-ssl.devops.local.conf
```
Содержание файла конфигурации:

```
<VirtualHost *:443>
ServerName test-ssl.devops.local
DocumentRoot /var/www/test-ssl.devops.local
SSLEngine on
SSLCertificateFile /etc/ssl/certs/server.pem
SSLCertificateKeyFile /etc/ssl/private/server.key
</VirtualHost>
```

5. Создаём страницу нашего тестового сайта:

Создаём каталог для сайта: `sudo mkdir /var/www/test-ssl.devops.local`
Создаём страницу: `sudo vi /var/www/test-ssl.devops.local/index.html`

Содержание страницы:

```
<h1>it worked!</h1>
```

6. Активируем конфиг и перезапускаем сервис:

```
sudo a2ensite test-ssl.devops.local.conf
sudo apache2ctl configtest
sudo systemctl reload apache2
```
Так же вносим изменения в файл /etc/hosts:

```
192.168.122.31 test-ssl.devops.local
```

где 192.168.122.31 - ip адрес моей виртуальной машины.

7. Проверяем `curl -v -k https://test-ssl.devops.local`:

```
*   Trying 192.168.122.31:443...
* Connected to test-ssl.devops.local (192.168.122.31) port 443 (#0)
* ALPN, offering h2
* ALPN, offering http/1.1
* TLSv1.0 (OUT), TLS header, Certificate Status (22):
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.2 (IN), TLS header, Certificate Status (22):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS header, Finished (20):
* TLSv1.2 (IN), TLS header, Supplemental data (23):
* TLSv1.3 (IN), TLS handshake, Encrypted Extensions (8):
* TLSv1.2 (IN), TLS header, Supplemental data (23):
* TLSv1.3 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS header, Supplemental data (23):
* TLSv1.3 (IN), TLS handshake, CERT verify (15):
* TLSv1.2 (IN), TLS header, Supplemental data (23):
* TLSv1.3 (IN), TLS handshake, Finished (20):
* TLSv1.2 (OUT), TLS header, Finished (20):
* TLSv1.3 (OUT), TLS change cipher, Change cipher spec (1):
* TLSv1.2 (OUT), TLS header, Supplemental data (23):
* TLSv1.3 (OUT), TLS handshake, Finished (20):
* SSL connection using TLSv1.3 / TLS_AES_256_GCM_SHA384
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: C=RU; ST=Some-State; O=Internet Widgits Pty Ltd; CN=test-ssl.devops.local
*  start date: Jun 19 10:38:15 2022 GMT
*  expire date: Jul 19 10:38:15 2022 GMT
*  issuer: C=RU; ST=Some-State; O=Internet Widgits Pty Ltd; CN=test-ssl.devops.local
*  SSL certificate verify result: self-signed certificate (18), continuing anyway.
* TLSv1.2 (OUT), TLS header, Supplemental data (23):
> GET / HTTP/1.1
> Host: test-ssl.devops.local
> User-Agent: curl/7.81.0
> Accept: */*
>
* TLSv1.2 (IN), TLS header, Supplemental data (23):
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
* TLSv1.2 (IN), TLS header, Supplemental data (23):
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
* old SSL session ID is stale, removing
* TLSv1.2 (IN), TLS header, Supplemental data (23):
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Sun, 19 Jun 2022 10:57:13 GMT
< Server: Apache/2.4.52 (Ubuntu)
< Last-Modified: Sun, 19 Jun 2022 10:50:15 GMT
< ETag: "14-5e1cac0ad3e9d"
< Accept-Ranges: bytes
< Content-Length: 20
< Content-Type: text/html
<
<h1>it worked!</h1>
* Connection #0 to host test-ssl.devops.local left intact
```

> 4. Проверьте на TLS уязвимости произвольный сайт в интернете (кроме сайтов МВД, ФСБ, МинОбр, НацБанк, РосКосмос, РосАтом, РосНАНО и любых госкомпаний, объектов КИИ, ВПК ... и тому подобное).

Скачиваем репозитарий со скриптом для проверки уязвимостей:

```
git clone --depth 1 https://github.com/drwetter/testssl.sh.git
```
Переходим в каталог `cd testssl.sh` и запускаем скрипт `./testssl.sh -U --sneaky https://tavria-rus.ru` 

Вывод результатов работы скрипта:

```
###########################################################
    testssl.sh       3.1dev from https://testssl.sh/dev/
    (13298ff 2022-06-01 09:47:12)

      This program is free software. Distribution and
             modification under GPLv2 permitted.
      USAGE w/o ANY WARRANTY. USE IT AT YOUR OWN RISK!

       Please file bugs @ https://testssl.sh/bugs/

###########################################################

 Using "OpenSSL 1.0.2-chacha (1.0.2k-dev)" [~179 ciphers]
 on bav-vm:./bin/openssl.Linux.x86_64
 (built: "Jan 18 17:12:17 2019", platform: "linux-x86_64")


 Start 2022-06-19 11:57:13        -->> 193.107.237.101:443 (tavria-rus.ru) <<--

 rDNS (193.107.237.101): vm-f9f5e986.na4u.ru.
 Service detected:       HTTP


 Testing vulnerabilities

 Heartbleed (CVE-2014-0160)                not vulnerable (OK), no heartbeat extension
 CCS (CVE-2014-0224)                       not vulnerable (OK)
 Ticketbleed (CVE-2016-9244), experiment.  not vulnerable (OK)
 ROBOT                                     Server does not support any cipher suites that use RSA key transport
 Secure Renegotiation (RFC 5746)           supported (OK)
 Secure Client-Initiated Renegotiation     not vulnerable (OK)
 CRIME, TLS (CVE-2012-4929)                not vulnerable (OK)
 BREACH (CVE-2013-3587)                    potentially NOT ok, "gzip" HTTP compression detected. - only supplied "/" tested
                                           Can be ignored for static pages or if no secrets in the page
 POODLE, SSL (CVE-2014-3566)               not vulnerable (OK)
 TLS_FALLBACK_SCSV (RFC 7507)              No fallback possible (OK), no protocol below TLS 1.2 offered
 SWEET32 (CVE-2016-2183, CVE-2016-6329)    not vulnerable (OK)
 FREAK (CVE-2015-0204)                     not vulnerable (OK)
 DROWN (CVE-2016-0800, CVE-2016-0703)      not vulnerable on this host and port (OK)
                                           make sure you don't use this certificate elsewhere with SSLv2 enabled services, see
                                           https://search.censys.io/search?resource=hosts&virtual_hosts=INCLUDE&q=71447533C92513CDE6B7432B3A64FAC892ED7C6633C79448E82D1560ABB29943
 LOGJAM (CVE-2015-4000), experimental      not vulnerable (OK): no DH EXPORT ciphers, no common prime detected
 BEAST (CVE-2011-3389)                     not vulnerable (OK), no SSL3 or TLS1
 LUCKY13 (CVE-2013-0169), experimental     not vulnerable (OK)
 Winshock (CVE-2014-6321), experimental    not vulnerable (OK) - ARIA, CHACHA or CCM ciphers found
 RC4 (CVE-2013-2566, CVE-2015-2808)        no RC4 ciphers detected (OK)


 Done 2022-06-19 11:57:34 [  23s] -->> 193.107.237.101:443 (tavria-rus.ru) <<--
```

> 5. Установите на Ubuntu ssh сервер, сгенерируйте новый приватный ключ. Скопируйте свой публичный ключ на другой сервер. Подключитесь к серверу по SSH-ключу.

1. Устанавливаем сервер: 

```
apt install openssh-server
systemctl start sshd.service
systemctl enable sshd.service
```
2. Генерируем ключи:

`ssh-keygen` без параметров.
Утилита предложит ввести имя и парольныю фразу для генерируемых ключей. По умолчанию имя будет выглядеть так:
* для приватного ключа id_<наименование_алгоритма> (по умолчанию rsa)
* для публичного ключа id_<наименование_алгоритма>.pub
Ключи будут сохранены в `~/.ssh/`

3. Копируем ключи на другой сервер:
   1. можно использовать утилиту `ssh-copy-id`. Пример: ssh-copy-id user@192.168.1.2 (в данном случае копируется файл id_rsa.pub, если мужно скопировать публичные ключи которые лежат в отличном от каталога по умолчанию месте можно использовать ключ `-i`).
   2. копируем вручную в файл authorized_keys содержимое .pub файла

4. Подключаемся по ключу:
   1. если ключи лежат в каталоге по умолчанию, то можно использовать `ssh user@<IP сервера>`
   2. если нужно использовать кастомные ключи то `ssh -i <путь до ключа> user@<IP адрес сервера>`

> 6. Переименуйте файлы ключей из задания 5. Настройте файл конфигурации SSH клиента, так чтобы вход на удаленный сервер осуществлялся по имени сервера.

1. Переименовываем файлы ключей `mv ~/.ssh/id_rsa ~/.ssh/test && mv ~/.ssh/id_rsa.pub ~/.ssh/test.pub`
2. Настраиваем файл конфигурации SSH клиента:
   1. `touch ~/.ssh/config && chmod 600 ~/.ssh/config`
   2. Приводим файл config к следующему виду:

```
host 192.168.1.2
 HostName 192.168.1.2
 IdentityFile ~/.ssh/test
 User user
```

> 7. Соберите дамп трафика утилитой tcpdump в формате pcap, 100 пакетов. Откройте файл pcap в Wireshark.

Сканирование запускается командой: `tcpdump -i enp1s0  -c 100 -w test.pcap`
Файл test.pcap открыл через Wireshark.

> 8*. Просканируйте хост scanme.nmap.org. Какие сервисы запущены?

Запускаем подробное сканирование командой: `nmap -A scanme.nmap.org`

```
Starting Nmap 7.80 ( https://nmap.org ) at 2022-06-19 14:47 UTC
Nmap scan report for scanme.nmap.org (45.33.32.156)
Host is up (0.19s latency).
Other addresses for scanme.nmap.org (not scanned): 2600:3c01::f03c:91ff:fe18:bb2f
Not shown: 995 closed ports
PORT      STATE    SERVICE    VERSION
22/tcp    open     ssh        OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   1024 ac:00:a0:1a:82:ff:cc:55:99:dc:67:2b:34:97:6b:75 (DSA)
|   2048 20:3d:2d:44:62:2a:b0:5a:9d:b5:b3:05:14:c2:a6:b2 (RSA)
|   256 96:02:bb:5e:57:54:1c:4e:45:2f:56:4c:4a:24:b2:57 (ECDSA)
|_  256 33:fa:91:0f:e0:e1:7b:1f:6d:05:a2:b0:f1:54:41:56 (ED25519)
25/tcp    filtered smtp
80/tcp    open     http       Apache httpd 2.4.7 ((Ubuntu))
|_http-server-header: Apache/2.4.7 (Ubuntu)
|_http-title: Go ahead and ScanMe!
9929/tcp  open     nping-echo Nping echo
31337/tcp open     tcpwrapped
Aggressive OS guesses: Linux 2.6.32 or 3.10 (96%), Linux 2.6.32 (95%), Linux 4.4 (95%), Linux 2.6.32 - 2.6.35 (94%), Linux 2.6.32 - 2.6.39 (94%), Linux 4.0 (93%), Linux 3.11 - 4.1 (92%), Linux 2.6.18 (92%), Linux 2.6.32 - 3.0 (92%), Linux 3.2 - 3.8 (91%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 12 hops
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 995/tcp)
HOP RTT       ADDRESS
1   0.37 ms   192.168.122.1
2   1.08 ms   172.16.77.1
3   3.64 ms   100.125.0.1
4   ... 5
6   22.06 ms  195.14.62.67
7   22.50 ms  217.161.97.25
8   41.15 ms  ae11.cr3-stk3.ip4.gtt.net (77.67.80.117)
9   188.62 ms ae3.cr5-sjc1.ip4.gtt.net (89.149.180.38)
10  189.66 ms ip4.gtt.net (208.116.213.134)
11  188.63 ms if-2-6.csw5-fnc1.linode.com (173.230.159.71)
12  191.44 ms scanme.nmap.org (45.33.32.156)

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 39.21 seconds
```

Запущены: ssh, Apache

> 9*. Установите и настройте фаервол ufw на web-сервер из задания 3. Откройте доступ снаружи только к портам 22,80,443

1. Устанавливааем ufw `apt install ufw`
2. Просматриваем все сетевые интерфейсы командой `ip addr`

```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:99:52:ac brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.31/24 metric 100 brd 192.168.122.255 scope global dynamic enp1s0
       valid_lft 2081sec preferred_lft 2081sec
    inet6 fe80::5054:ff:fe99:52ac/64 scope link
       valid_lft forever preferred_lft forever
```

Сбрасываем правила:

```
    sudo ufw default deny incoming
    sudo ufw default allow outgoing
```

Добавляем правила:

```
   sudo ufw allow in on enp1s0 to any port 22
   sudo ufw allow in on enp1s0 to any port 80
   sudo ufw allow in on enp1s0 to any port 443
```

Активируем ufw `sudo ufw enable` и просматриваем созданные правила `sudo ufw status numbered`

```
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] 22 on enp1s0               ALLOW IN    Anywhere
[ 2] 80 on enp1s0               ALLOW IN    Anywhere
[ 3] 443 on enp1s0              ALLOW IN    Anywhere
[ 4] 22 (v6) on enp1s0          ALLOW IN    Anywhere (v6)
[ 5] 80 (v6) on enp1s0          ALLOW IN    Anywhere (v6)
[ 6] 443 (v6) on enp1s0         ALLOW IN    Anywhere (v6)
```